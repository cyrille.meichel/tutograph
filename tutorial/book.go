package tutorial

import (
	"math"
	"regexp"
)

// Book is a book
type Book struct {
	ID       string  `json:"id,omitempty"`
	Title    string  `json:"title"`
	Genre    string  `json:"genre"`
	Price    float64 `json:"price"`
	AuthorID string  `json:"authorId"`
}

// IsAuthor checks if the bokk has this author
func (book Book) IsAuthor(author Author) bool {
	return book.AuthorID == author.ID
}

// HasGenre checks the genre of the book
func (book Book) HasGenre(genre string) bool {
	return regexp.MustCompile("(?i)" + genre).MatchString(book.Genre)
}

// HasTitle checks the title of the book
func (book Book) HasTitle(title string) bool {
	return regexp.MustCompile("(?i)" + title).MatchString(book.Title)
}

// PriceCompare compares price:
// 0:  prices are equal
// -1: price is lower
// +1: price is over
func (book Book) PriceCompare(price float64) int {
	if book.Price == price {
		return 0
	}
	return int(math.Round((book.Price - price) / math.Abs(book.Price-price)))
}

package tutorial_test

import (
	"testing"

	"gitlab.com/cyrille.meichel/tutograph/tutorial"
)

func TestAuthorHasWritten(t *testing.T) {
	author := tutorial.Author{
		ID:      "0",
		Name:    "Enid Blyton",
		Email:   "enid.blyton@gmail.com",
		BooksID: []string{"4", "8"},
	}

	if author.HasWritten(tutorial.Book{ID: "5"}) {
		t.Errorf("Book not written by this author")
		return
	}

	if !author.HasWritten(tutorial.Book{ID: "8"}) {
		t.Errorf("Book written by this author")
		return
	}
}

func TestAuthorHasName(t *testing.T) {
	author := tutorial.Author{
		ID:      "0",
		Name:    "Enid Blyton",
		Email:   "enid.blyton@gmail.com",
		BooksID: []string{"4", "8"},
	}

	if author.HasName("azerty") {
		t.Errorf("Author name does not match")
		return
	}

	if !author.HasName("bly") {
		t.Errorf("Author name matches")
		return
	}
}

func TestAuthorHasEMail(t *testing.T) {
	author := tutorial.Author{
		ID:      "0",
		Name:    "Enid Blyton",
		Email:   "enid.blyton@gmail.com",
		BooksID: []string{"4", "8"},
	}

	if author.HasEmail("azerty") {
		t.Errorf("Author email does not match")
		return
	}

	if !author.HasEmail("Eni") {
		t.Errorf("Author email matches")
		return
	}
}

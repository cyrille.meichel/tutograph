package tutorial_test

import (
	"testing"

	"gitlab.com/cyrille.meichel/tutograph/tutorial"
)

func TestBookIsAuthor(t *testing.T) {
	book := tutorial.Book{
		ID:       "0",
		Title:    "Martine carbure au LSD",
		Genre:    "children",
		Price:    10.0,
		AuthorID: "2",
	}

	if book.IsAuthor(tutorial.Author{ID: "1"}) {
		t.Errorf("Book not written by this author")
		return
	}

	if !book.IsAuthor(tutorial.Author{ID: "2"}) {
		t.Errorf("Book written by this author")
		return
	}
}

func TestBookHasTitle(t *testing.T) {
	book := tutorial.Book{
		ID:       "0",
		Title:    "Martine carbure au LSD",
		Genre:    "children",
		Price:    10.0,
		AuthorID: "2",
	}

	if book.HasTitle("azerty") {
		t.Errorf("Book title does not match")
		return
	}

	if !book.HasTitle("martine") {
		t.Errorf("Book title matches")
		return
	}
}

func TestBookHasGenre(t *testing.T) {
	book := tutorial.Book{
		ID:       "0",
		Title:    "Martine carbure au LSD",
		Genre:    "children",
		Price:    10.0,
		AuthorID: "2",
	}

	if book.HasGenre("detective") {
		t.Errorf("Book title does not match")
		return
	}

	if !book.HasGenre("child") {
		t.Errorf("Book title matches")
		return
	}
}

func TestBookPriceCompare(t *testing.T) {
	book := tutorial.Book{
		ID:       "0",
		Title:    "Martine carbure au LSD",
		Genre:    "children",
		Price:    10.3,
		AuthorID: "2",
	}

	if book.PriceCompare(10.0) != 1 {
		t.Errorf("Book is more expansive")
		return
	}

	if book.PriceCompare(10.3) != 0 {
		t.Errorf("Book is same price")
		return
	}

	if book.PriceCompare(10.5) != -1 {
		t.Errorf("Book is less expansive")
		return
	}
}

package tutorial

import "regexp"

// Author is a book author
type Author struct {
	ID      string   `json:"id,omitempty"`
	Name    string   `json:"name"`
	Email   string   `json:"email"`
	BooksID []string `json:"booksId"`
}

// HasWritten checks if the author wrote the book
func (author Author) HasWritten(book Book) bool {
	for _, id := range author.BooksID {
		if id == book.ID {
			return true
		}
	}
	return false
}

// HasName checks the name of the author
func (author Author) HasName(name string) bool {
	return regexp.MustCompile("(?i)" + name).MatchString(author.Name)
}

// HasEmail checks the email of the author
func (author Author) HasEmail(email string) bool {
	return regexp.MustCompile("(?i)" + email).MatchString(author.Email)
}

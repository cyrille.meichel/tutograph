# Main.go

## Create struct `QueryRequest`

```go
//QueryRequest is the query from client
type QueryRequest struct {
       Query string `json:"query"`
}

```

## Declare host/port

```go
var port = 9000
var hostname = "0.0.0.0"
```

## create http server

```go
func main() {
    http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
        writer.Header().Set("Content-Type", "application/json")
        
        // Read body
        bytes, errBody := ioutil.ReadAll(request.Body)
        defer request.Body.Close()
        if errBody != nil {
                http.Error(writer, errBody.Error(), http.StatusInternalServerError)
                return
        }

        // build the request
        query := QueryRequest{}
        errJSON := json.Unmarshal(bytes, &query)
        if errJSON != nil {
                http.Error(writer, errJSON.Error(), http.StatusInternalServerError)
                return
        }

        // return a result
        result := map[string]string{
                "result": query.Query,
        }
        json.NewEncoder(writer).Encode(result)
    })
    fmt.Printf("Listening on %s:%d\n", host, port)
    http.ListenAndServe(fmt.Sprintf("%s:%d", host, port), nil)
}
```
package main

import (
	"fmt"

	"gitlab.com/cyrille.meichel/tutograph/internal/database"
)

func main() {
	fmt.Printf("Hi, this is the starting point of a tutorial on graphQL\n")
	fmt.Printf("Authors:\n")
	for _, author := range database.Authors {
		fmt.Printf("  * %v\n", author)
	}
	fmt.Printf("Books:\n")
	for _, book := range database.Books {
		fmt.Printf("  * %v\n", book)
	}
}

GO=go
SOURCES := $(shell find ../.. -name '*.go' -not -path 'vendor')
ROOT_PATH:=$(shell cd ../../.. && pwd)

EXEC=tutograph

$(EXEC): $(SOURCES)
	$(GO) build -o $@

test:
	ROOT_PATH=$(ROOT_PATH) $(GO) test -v ./...

clean:
	rm -f $(EXEC)
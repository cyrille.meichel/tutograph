package database_test

import (
	"testing"

	"gitlab.com/cyrille.meichel/tutograph/internal/database"
	"gitlab.com/cyrille.meichel/tutograph/tutorial"
)

func TestBookFilter(t *testing.T) {
	var filteredBooks []tutorial.Book
	filteredBooks = database.Books.Filter(func(book tutorial.Book) bool { return true })
	if len(filteredBooks) != len(database.Books) {
		t.Error("Blank filter should give the whole set of books")
	}

	filteredBooks = database.Books.Filter(func(book tutorial.Book) bool { return book.ID == "0" })
	if len(filteredBooks) != 1 {
		t.Error("Filter by ID should return on element")
	}
}

func TestBookCount(t *testing.T) {

	if database.Books.Count(func(book tutorial.Book) bool { return true }) != len(database.Books) {
		t.Error("Blank filter should give the whole set of books")
	}

	if database.Books.Count(func(book tutorial.Book) bool { return book.ID == "0" }) != 1 {
		t.Error("Filter by ID should return on element")
	}
}

func TestBookGetById(t *testing.T) {
	book, err := database.Books.GetByID("0")
	if err != nil {
		t.Error(err)
		return
	}
	if book.ID != "0" {
		t.Error("That's not the correct book !")
	}
}

func TestBookInsert(t *testing.T) {
	data := database.Books.Insert(tutorial.Book{
		Title: "Les anges se font plumer",
		Genre: "detective",
		Price: 6.4,
	})
	book, err := database.Books.GetByID(data.ID)
	if err != nil {
		t.Error(err)
		return
	}
	if book.Title != "Les anges se font plumer" {
		t.Error("That's not the correct book !")
	}
	if book.Genre != "detective" {
		t.Error("That's not the correct book !")
	}
	if book.Price != 6.4 {
		t.Error("That's not the correct book !")
	}
}

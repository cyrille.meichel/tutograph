package database_test

import (
	"testing"

	"gitlab.com/cyrille.meichel/tutograph/internal/database"
	"gitlab.com/cyrille.meichel/tutograph/tutorial"
)

func TestAuthorFilter(t *testing.T) {
	var filteredAuthors []tutorial.Author
	filteredAuthors = database.Authors.Filter(func(author tutorial.Author) bool { return true })
	if len(filteredAuthors) != len(database.Authors) {
		t.Error("Blank filter should give the whole set of authors")
	}

	filteredAuthors = database.Authors.Filter(func(author tutorial.Author) bool { return author.ID == "0" })
	if len(filteredAuthors) != 1 {
		t.Error("Filter by ID should return on element")
	}
}

func TestAuthorCount(t *testing.T) {

	if database.Authors.Count(func(author tutorial.Author) bool { return true }) != len(database.Authors) {
		t.Error("Blank filter should give the whole set of authors")
	}

	if database.Authors.Count(func(author tutorial.Author) bool { return author.ID == "0" }) != 1 {
		t.Error("Filter by ID should return on element")
	}
}

func TestAuthorGetById(t *testing.T) {
	author, err := database.Authors.GetByID("0")
	if err != nil {
		t.Error(err)
		return
	}
	if author.ID != "0" {
		t.Error("That's not the correct author !")
	}
}

func TestAuthorInsert(t *testing.T) {
	data := database.Authors.Insert(tutorial.Author{
		Name:  "Frédéric Dard",
		Email: "frederic.dard@gmail.com",
	})
	author, err := database.Authors.GetByID(data.ID)
	if err != nil {
		t.Error(err)
		return
	}
	if author.Name != "Frédéric Dard" {
		t.Error("That's not the correct author !")
	}
	if author.Email != "frederic.dard@gmail.com" {
		t.Error("That's not the correct author !")
	}
}

package database

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/cyrille.meichel/tutograph/tutorial"
)

// authorDB is the simulated database type
type authorDB []tutorial.Author

// Filter filters data from DB
func (db *authorDB) Filter(f func(tutorial.Author) bool) []tutorial.Author {
	vsf := make([]tutorial.Author, 0)
	for _, v := range *db {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

// Count counts objects
func (db *authorDB) Count(f func(tutorial.Author) bool) int {
	count := 0
	for _, v := range *db {
		if f(v) {
			count++
		}
	}
	return count
}

// GetByID gets an author by it Identifier
func (db *authorDB) GetByID(ID string) (tutorial.Author, error) {
	filtered := db.Filter(func(author tutorial.Author) bool {
		return author.ID == ID
	})
	if len(filtered) == 1 {
		return filtered[0], nil
	}

	return tutorial.Author{}, fmt.Errorf("No Author %s", ID)
}

// Insert appends an author in the database
func (db *authorDB) Insert(data tutorial.Author) tutorial.Author {
	data.ID = uuid.NewV4().String()
	*db = append(*db, data)
	return data
}

// Authors is the simulated author database
var Authors = authorDB{
	tutorial.Author{
		ID:      "0",
		Name:    "Enid Blyton",
		Email:   "enid.blyton@gmail.com",
		BooksID: []string{"4"},
	},
	tutorial.Author{
		ID:      "1",
		Name:    "Gotlib",
		Email:   "gotlib@gmail.com",
		BooksID: []string{"5"},
	},
	tutorial.Author{
		ID:      "2",
		Name:    "Gilbert Delahaye",
		Email:   "gilbert.delahaye@gmail.com",
		BooksID: []string{"0", "1"},
	},
	tutorial.Author{
		ID:      "3",
		Name:    "Marion Montaigne",
		Email:   "marion.montaigne@gmail.com",
		BooksID: []string{"2", "3"},
	},
}

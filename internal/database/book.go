package database

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/cyrille.meichel/tutograph/tutorial"
)

// bookDB is the simulated database type
type bookDB []tutorial.Book

// Filter filters data from DB
func (db *bookDB) Filter(f func(tutorial.Book) bool) []tutorial.Book {
	vsf := make([]tutorial.Book, 0)
	for _, v := range *db {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

// Count counts objects
func (db *bookDB) Count(f func(tutorial.Book) bool) int {
	count := 0
	for _, v := range *db {
		if f(v) {
			count++
		}
	}
	return count
}

// GetByID gets an book by it Identifier
func (db *bookDB) GetByID(ID string) (tutorial.Book, error) {
	filtered := db.Filter(func(book tutorial.Book) bool {
		return book.ID == ID
	})
	if len(filtered) == 1 {
		return filtered[0], nil
	}

	return tutorial.Book{}, fmt.Errorf("No tutorial.Book")
}

// Insert appends an book in the database
func (db *bookDB) Insert(data tutorial.Book) tutorial.Book {
	data.ID = uuid.NewV4().String()
	*db = append(*db, data)
	return data
}

// Books is the simulated book database
var Books = bookDB{
	tutorial.Book{
		ID:       "0",
		Title:    "Martine carbure au LSD",
		Genre:    "children",
		Price:    10.0,
		AuthorID: "2",
	},
	tutorial.Book{
		ID:       "1",
		Title:    "Martine au parc",
		Genre:    "children",
		Price:    11.0,
		AuthorID: "2",
	},
	tutorial.Book{
		ID:       "2",
		Title:    "Tu mourras moins bête",
		Genre:    "Comic",
		Price:    15.0,
		AuthorID: "3",
	},
	tutorial.Book{
		ID:       "3",
		Title:    "Dans la combi de Tomas Pesquet",
		Genre:    "Comic",
		Price:    20.0,
		AuthorID: "3",
	},
	tutorial.Book{
		ID:       "4",
		Title:    "Oui-oui et son taxi",
		Genre:    "children",
		Price:    5.0,
		AuthorID: "0",
	},
	tutorial.Book{
		ID:       "5",
		Title:    "Dingodossier",
		Genre:    "Comic",
		Price:    5.0,
		AuthorID: "1",
	},
}

# Tutorial: How to build a simple graphQL API

## Prerequisites

You must have a sane installation of Go (https://golang.org/doc/install)

> Extensions exists on browsers (with autodiscover of the API schema):
> 
> * Chromium: https://chrome.google.com/webstore/detail/graphql-playground-for-ch/kjhjcgclphafojaeeickcokfbhlegecd?hl=en-GB
>
> * FireFox: https://addons.mozilla.org/fr/firefox/addon/altair-graphql-client/

## Compile

```bash
make
```

## Test
```bash
make test
```

## Application

### Models

This example has 2 models:
* Author
* Book

I built the models to make a vicious circle, to show the power of graphQL: 
 * `Author` has a reference to `Book`
 * `Book` has a reference to `Author`

#### Author

```java
{
    id:      String
	name:    String
	email:   String
    books:   Book[]
}
```

#### Book

```java
{
    id:       String
	title:    String
	genre:    String
	price:    float
	author:   Author
}
```

## Tutorial

### 1. Present the data model

### 2. Create http server that reads body
#### 2.1 Basic server
#### 2.2 Read Body

### 3. Create graphql schema for requesting books and authors
#### 3.1 Body object
#### 3.2 Basic structure
#### 3.3 Show queries

### 4. Send data from the database
#### 4.1 Connect to the database
#### 4.2 Make the vicious circle

### 5. Filter the results
#### 5.1 Arguments in queries
#### 5.2 Resolve

#### 6. Mutations: How to modify data
#### 6.1 The mess in arguments !
#### 6.2 Input variable

## GraphQL Model
```mermaid
graph TD
	classDef PropClass fill:#dfff80,stroke:#86b300;
	classDef TypeClassOutput fill:#99e6ff,stroke:#0086b3;
	classDef TypeClassInput fill:#ffdf80,stroke:#806000;
	classDef Types fill:#d9b3ff,stroke:#6600cc;
	classDef NamedField fill:#ffff80,stroke:#999900;
	Schema[Schema] -->|NewSchema|SchemaConfig[SchemaConfig]
	SchemaConfig --> QueryProp(Query)
	SchemaConfig --> MutationProp(Mutation)
	

	subgraph Object 'Output'
	    QueryProp --> QueryObject{"Output"}
		QueryProp --> |NewList|QueryListObject{"Output[]"}

        QueryListObject --> |NewObject|ObjectConfig[ObjectConfig]
		QueryObject --> |NewObject|ObjectConfig[ObjectConfig]
		ObjectConfig -->NameProp(Name)
		ObjectConfig -->DescriptionProp(Description)
		ObjectConfig -->FieldsProp(Fields)

		FieldsProp-->Fields[Fields]
		Fields-->AnyProp0(foo 0)
		Fields-->AnyProp1(foo 1)
		AnyProp1 --> Field[*Field]

		Field --> NameProp1(Name)
		Field --> DescriptionProp1(Description)
		Field --> ArgsProp1(Args)
		Field -->ResolveProp1("Resolve(params ResolveParams) => (interface{}, error)")

		ArgsProp1 --> FieldConfigArgument[FieldConfigArgument]
		
	end

	subgraph Object 'Output'
	    MutationProp --> MutationObject{"Output"}
		MutationProp --> |NewList|MutationListObject{"Output[]"}
	end

	subgraph Object 'Output'
		Field --> TypeProp1(Type)
		TypeProp1 --> OtherObject{"Output"}
		TypeProp1 --> |NewList|ListObject{"Output[]"}
		TypeProp1 --> Scalar{"Scalar"}
	end

	subgraph Arguments
		FieldConfigArgument --> ArgumentsProp0(foo 0)
		FieldConfigArgument --> ArgumentsProp1(foo 1)
		ArgumentsProp1 --> ArgumentConfig[*ArgumentConfig]
		ArgumentConfig --> ArgumentConfigDefaultValue(DefaultValue)
		ArgumentConfig --> ArgumentConfigDescription(Description)
		ArgumentConfig --> ArgumentConfigType(Type)

		ArgumentConfigType --> ArgsOtherInput{"Input"}
		ArgumentConfigType --> |NewNonNull|ArgsOtherInputNonNull{"Input!"}
		ArgumentConfigType --> |NewList|ArgsListObject{"Input[]"}
		ArgumentConfigType --> ArgsScalarObject{"Scalar"}

	end

    subgraph Object Input
		ArgsOtherInput --> InputObject[*InputObject]
        InputObject --> |NewInputObject|InputObjectConfig[InputObjectConfig]
        InputObjectConfig --> InputObjectConfigName(Name)
        InputObjectConfig --> InputObjectConfigDescription(Description)
        InputObjectConfig --> InputObjectConfigFields(Fields)
        InputObjectConfigFields --> InputObjectConfigFieldMap[InputObjectConfigFieldMap]
		InputObjectConfigFieldMap --> InputField0("foo 0")
		InputObjectConfigFieldMap --> InputField1("foo 1")
		InputField1 --> InputObjectFieldConfig[*InputObjectFieldConfig]
		InputObjectFieldConfig --> InputObjectFieldConfigDescription(Description)
		InputObjectFieldConfig --> InputObjectFieldConfigDefaultValue(DefaultValue)
		InputObjectFieldConfig --> InputObjectFieldConfigType(Type)
		InputObjectFieldConfigType --> InputObjectFieldConfigTypeInput{Input}
		InputObjectFieldConfigType --> |NewNonNull|InputObjectFieldConfigTypeInputNonNull{"Input!"}
		InputObjectFieldConfigType --> |NewList|InputObjectFieldConfigTypeInputList{"Input[]"}
		InputObjectFieldConfigType --> InputObjectFieldConfigTypeInputScalar{"Scalar"}
    end

	class QueryProp,MutationProp,NameProp,DescriptionProp,FieldsProp,NameProp1,TypeProp1,DescriptionProp1,ArgsProp1,ResolveProp1,ArgumentConfigDefaultValue,ArgumentConfigDescription,ArgumentConfigType,InputObjectConfigName,InputObjectConfigDescription,InputObjectConfigFields,InputObjectFieldConfigDescription,InputObjectFieldConfigDefaultValue,InputObjectFieldConfigType PropClass;
	
	class OtherObject,ListObject,Scalar,QueryObject,QueryListObject,MutationObject,MutationListObject TypeClassOutput;
	
	class ArgsOtherInput,ArgsListObject,InputObjectFieldConfigTypeInput,InputObjectFieldConfigTypeInputList,InputObjectFieldConfigTypeInputNonNull,ArgsOtherInputNonNull,InputObjectFieldConfigTypeInputScalar,ArgsScalarObject TypeClassInput

	class AnyProp0,AnyProp1,ArgumentsProp0,ArgumentsProp1,InputField0,InputField1 NamedField;

	class Schema,SchemaConfig,ObjectConfig,Fields,Field,FieldConfigArgument,ArgumentConfig,InputObject,InputObjectConfig,InputObjectConfigFieldMap,InputObjectFieldConfig Types;
```